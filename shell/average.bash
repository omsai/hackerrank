# https://www.hackerrank.com/challenges/bash-tutorials---compute-the-average/problem
readarray -t nums <&0
n=${nums[0]}
unset nums[0]
sum=0
for num in ${nums[*]}; do
    sum=$(( sum + num ))
done
bc -l <<< "$sum / $n" | xargs printf "%.3f\\n"
