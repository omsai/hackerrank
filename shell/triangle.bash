# https://www.hackerrank.com/challenges/bash-tutorials---more-on-conditionals/problem
readarray -t sides < /dev/stdin
i=1
nequal=0
for side in ${sides[*]}; do
    if [[ $i -ne 1 ]]; then
	if [[ $side -eq $last_side ]]; then
	    nequal=$(( nequal + 1 ))
	fi
    fi
    last_side=$side
    i=$(( i + 1 ))
done
case $nequal in
    0) echo "SCALENE";;
    1) echo "ISOSCELES";;
    2) echo "EQUILATERAL";;
esac
