# https://www.hackerrank.com/challenges/bash-tutorials---arithmetic-operations/problem
read -r line
bc -l <<< "$line" | xargs printf "%.3f\\n"
