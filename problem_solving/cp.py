from typing import (
    Any, Iterable, Optional, Set, Callable, Generator, Dict, List, Tuple)
from abc import abstractmethod, ABC
from pprint import pprint, pformat
import inspect                  # function names

__author__ = "Eugene Kovalev"


def first(collection: Iterable[Any]) -> Optional[Any]:
    for i in collection:
        return i
    return None


class IntVar():
    """A very simplistic, immutable object that contains a variable identifier
    (needs to be globally unique, otherwise shit won't work) and a set of
    integers that represents its domain. Once the domain shrinks to 1, the
    variable is set. A solution is a state where all variables have their
    domains of size exactly 1. If a variable's domain shrinks to a size less
    than 1, the state is considered invalid.

    """
    @staticmethod
    def of_var(var: 'IntVar') -> 'IntVar':
        return IntVar(var.identifier, set(var.domain))

    @staticmethod
    def of_ints(identifier: str, ints: Iterable[int]) -> 'IntVar':
        return IntVar(identifier, set(ints))

    @staticmethod
    def of_int(identifier: str, value: int) -> 'IntVar':
        return IntVar(identifier, {value})

    @staticmethod
    def of_range(identifier: str, start: int, end: int) -> 'IntVar':
        return IntVar.of_ints(identifier, range(start, end))

    def __init__(self, identifier, domain):
        self.identifier = identifier
        self.domain = domain

    def fixed_at(self, value: int) -> 'IntVar':
        if value in self.domain:
            return IntVar.of_int(self.identifier, value)
        raise ValueError("Attempted to set a variable to a value that is not"
                         " present in its domain")

    def is_fixed(self) -> bool:
        return len(self.domain) == 1

    def is_empty(self) -> bool:
        return len(self.domain) == 0

    def without(self, values: Set[Optional[int]]):
        return IntVar(self.identifier, {i for i in self.domain
                                        if i not in values})

    def value(self) -> int:
        if self.is_fixed():
            return next(i for i in self.domain)
        raise ValueError("Attempted to extract value from an unfixed "
                         "variable")

    def __repr__(self) -> str:
        return f'{self.identifier}[{", ".join([str(i) for i in self.domain])}]'


class State():
    """Nothing fancy... just a dictionary of variables. If you need your solver to
    handle optimization problems instead of just satisfaction problems, I
    recommend defining your objective functions as ones that map a state to a
    value that you wish to either maximize or minimize.

    """
    @staticmethod
    def first(model: 'Model') -> 'State':
        return State({v.identifier: v for v in model.variables})

    def __init__(self, variables: Dict[str, IntVar]):
        self.variables = variables

    def is_solution(self) -> bool:
        return all(v.is_fixed() for v in self.variables.values())

    def is_invalid(self) -> bool:
        return any(v.is_empty() for v in self.variables.values())

    def with_fixed_variable(self, identifier: str, fix: int) -> 'State':
        # clone this state with one variable set to a specific value
        return State({k: (v.fixed_at(fix)
                          if k == identifier
                          else IntVar.of_var(v))
                      for k, v in self.variables.items()})

    def domain_changed(self, other: 'State') -> bool:
        assert \
            set(self.variables.keys()) == set(other.variables.keys()), \
            'Cannot compare two states with different sets of variables... ' \
            'that should not even happen.'
        return any(self.variables[k].domain != other.variables[k].domain
                   for k in self.variables)

    def __repr__(self) -> str:
        return pformat(self.variables)


class Constraint(ABC):
    @abstractmethod
    def apply(self, state: State) -> Tuple[Optional[State], bool]:
        """If a constraint is violated return None."""
        return None, False


class AllDiff(Constraint):
    """A constraint is merely a class that is given a set of variable identifiers
    that it knows about and uses for its calculations.  The critical method is
    `apply()`. When a constraint is so-applied, it takes a state, which is just
    a collection of variables, and generates a new state where the domains of
    relevant variables have been shrunk in accordance with a constraint. For
    example, the all-diff constraint, would be initialized with a set of
    variable identifiers. When given a state, it would iterate through all of
    those variables and produce a cloned state where any values that have been
    assigned to a variable have been removed from the domains of all the other
    variables.  We're not going to call this implementation efficient, but it
    should get the point across.

    A thought occurs, one could consider "heuristics" as merely functions that
    define the order in which variables and constraints are evaluated during
    solving. One could modify the solve function to take two heuristic lambdas
    (or objects), one for the variable ordering during evaluation, and one for
    constraint ordering.

    """
    def __init__(self, variables: Set[str]):
        self.variables = variables

    def apply(self, state: State) -> Tuple[Optional[State], bool]:
        """Finds all the fixed values and removes them repeatedly until we no longer
        see a change in the set of fixed values.

        Returns either the constrained state, with a boolean indicating whether
        any domains have changed, or None indicating that the constraint was
        violated.

        Note that the alldiff constraint is more efficiently implemented using
        MaxFlow.

        """
        variables = {k: state.variables[k] for k in self.variables}
        domain_changed = False
        before = 0
        fixed_values: Dict[str, Optional[int]] = {
            var.identifier: first(var.domain)
            for var in variables.values()
            if var.is_fixed()
        }
        while before != len(fixed_values):
            # if we found new fixed values, run the constraint again
            before = len(fixed_values)  # set before to catch any changes
            for ident in self.variables:
                var = variables[ident].without({
                    v for k, v in fixed_values.items() if k != ident})
                if len(var.domain) != len(variables[ident].domain):
                    domain_changed = True
                    variables[ident] = var
                    if var.is_fixed():
                        fixed_values[ident] = first(var.domain)
                    if var.is_empty():
                        return None, domain_changed  # constraint is violated
        result = State({k: (IntVar.of_var(v)
                            if k not in variables
                            else variables[k])
                        for k, v in state.variables.items()})
        return result, domain_changed


class Model():
    def __init__(self, variables: Iterable[IntVar],
                 constraints: Iterable[Constraint]):
        self.variables = variables
        self.constraints = constraints


def propagate(state: State,
              constraints: Iterable[Constraint]) -> Optional[State]:
    """Propagates all the constraints until there are no more domain changes and
    returns a state that does not violate any constraints. If a constraint is
    violated, this function will return None

    """
    domain_changed = True
    current_state: Optional[State] = state
    # propagate all constraints repeatedly until no domains are changed
    while domain_changed:
        for constraint in constraints:
            if current_state:
                current_state, domain_changed = constraint.apply(current_state)
            else:
                return None
    return current_state


def valid_children_of(state: Optional[State],
                      constraints: Iterable[Constraint]) -> \
                      Generator[State, None, None]:
    """Try fixing every variable in the state to every value in its domain.  Yield
    only child states that satisfy all constraints (that way the stack doesn't
    get too full of states that are invalid)

    """
    if (state is not None and
        not state.is_solution() and
            not state.is_invalid()):
        for identifier, variable in state.variables.items():
            for value in variable.domain:
                valid_state = propagate(
                    state.with_fixed_variable(identifier, value), constraints)
                if valid_state is not None:
                    # pprint(('yielding valid state: ', valid_state))
                    yield valid_state


def dfs_solve(model: Model) -> Generator[State, None, None]:
    """This function generates solutions that you can loop over. You can either
    grab all of them, the first one, or attempt to maximize/minimize given an
    objective function. You can make a nearly identical BFS function by copying
    this one and changing the stack to a queue

    You can also introduce a heuristic lamdba that tells you which state to try
    next.

    """
    stack: List[State] = [State.first(model)]
    # propagate constraints on first state
    current_state: Optional[State] = (
        propagate(stack.pop(), model.constraints))
    if current_state is not None and current_state.is_solution():
        yield current_state
    # while the stack is not empty, pop a state off the stack
    while 42:
        # loop over all child states that satisfy the constraints
        for child_state in valid_children_of(current_state, model.constraints):
            if not child_state.is_invalid():
                if child_state.is_solution():
                    yield child_state  # yeild the child if its a solution
                else:
                    # if its not yet a solution, append the state to the stack
                    stack.append(child_state)
        if stack:
            current_state = stack.pop()
        else:
            break


def satisfy(model: Model) -> Optional[State]:
    """Solves a constraint satisfaction problem given a model

    Finds a solution that satisfies the given constraints of the
    model. A solution is a state where all variables have a
    domain of size 1. If a solution cannot be found, this function
    will return None.

    """
    for solution in dfs_solve(model):
        return solution
    return None


def maximize(model: Model, objective_function: Callable[[State], int]) -> \
        Optional[State]:
    """Solves a constraint optimization problem given a model and an objective
    function whose value it attempts to maximize.  If no state can be found
    that satisfies all the constraints, None is returned. An objective function
    is one that takes a State object and returns an int.

    """
    return max((solution
                for solution in dfs_solve(model)), key=objective_function)


def minimize(model: Model, objective_function: Callable[[State], int]) -> \
        Optional[State]:
    """
    Solves a constraint optimization problem given a model and
    an objective function whose value it attempts to minimize.
    If no state can be found that satisfies all the constraints,
    None is returned. An objective function is one that takes a
    State object and returns an int.
    """
    return min((solution
                for solution in dfs_solve(model)), key=objective_function)


def test_basic_solution():
    var = IntVar('A', [1, 2])
    model = Model({var}, [AllDiff(['A'])])
    solution = satisfy(model)
    assert solution is not None
    assert solution.variables['A'].domain == {1}
    print(f'{inspect.stack()[0][3]}: PASSED')


def test_all_diff_with_two_variables():
    var_a = IntVar('A', [1, 2])
    var_b = IntVar('B', [1, 2])
    model = Model([var_a, var_b], [AllDiff(['A', 'B'])])
    solution = satisfy(model)
    assert solution is not None
    assert solution.variables['A'].domain == {1}
    assert solution.variables['B'].domain == {2}
    print(f'{inspect.stack()[0][3]}: PASSED')


def test_unsatisfiable_constraint():
    var_a = IntVar('A', [1])
    var_b = IntVar('B', [1])
    model = Model([var_a, var_b], [AllDiff(['A', 'B'])])
    solution = satisfy(model)
    assert solution is None
    print(f'{inspect.stack()[0][3]}: PASSED')


def test_sudoku_row():
    var_names = [str(i) for i in range(1, 4)]
    variables = [IntVar.of_range(str(i), 1, 4) for i in var_names]
    variables[1] = variables[1].fixed_at(2)
    model = Model(variables, [AllDiff(var_names)])
    solution = satisfy(model)
    assert solution is not None
    pprint(solution)
    for i in range(1, 4):
        assert solution.variables[str(i)].domain == {i}
    print(f'{inspect.stack()[0][3]}: PASSED')


def test_4x4_sudoku():
    variables = [[IntVar.of_range(f'{r},{c}', 1, 5) for c in range(4)]
                 for r in range(4)]
    # fixate variables to their starting positions for the puzzle
    variables[0][0] = variables[0][0].fixed_at(1)
    variables[1][1] = variables[1][1].fixed_at(2)
    variables[2][2] = variables[2][2].fixed_at(3)
    variables[3][3] = variables[3][3].fixed_at(4)
    print(('\n' + '-' * 13 + '\n').join([
        (' | '.join(((str(first(v.domain)))
                     if v.is_fixed() else '?')
                    for v in row)) for row in variables]))
    rows = [AllDiff([f'{r},{c}' for c in range(4)]) for r in range(4)]
    cols = [AllDiff([f'{r},{c}' for r in range(4)]) for c in range(4)]
    # flatten variables
    model = Model([item for sublist in variables for item in sublist],
                  rows + cols)
    S = satisfy(model).variables
    print()
    print(('\n' + '-' * 13 + '\n').join([
        ' | '.join([str(S['{},{}'.format(i, j)].value())
                    for j in range(4)])
        for i in range(4)]))
    assert S['0,0'].value() == 1
    assert S['1,1'].value() == 2
    assert S['2,2'].value() == 3
    assert S['3,3'].value() == 4
    print(f'{inspect.stack()[0][3]}: PASSED')


def main():
    test_basic_solution()
    test_all_diff_with_two_variables()
    test_unsatisfiable_constraint()
    test_sudoku_row()
    test_4x4_sudoku()


if __name__ == '__main__':
    main()
