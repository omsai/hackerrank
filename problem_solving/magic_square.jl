# https://www.hackerrank.com/challenges/magic-square-forming/problem

# import Pkg
# Pkg.add("JuMP")
# Pkg.add("Cbc")

# Sample input 0 should have an objective function of 1
# s = [[4 9 2]
#      [3 5 7]
#      [8 1 5]]

# Sample input 1 should have an objective function of 4
s = [[4 8 2]
     [4 5 7]
     [6 1 6]]

using LinearAlgebra             # tr

using JuMP
using Cbc

model = Model(Cbc.Optimizer)

n = 3
@variable(model, x[1:n, 1:n], lower_bound=-n^2+1, upper_bound=n^2-1, integer=true)
@variable(model, y[1:n, 1:n], lower_bound=1,      upper_bound=n^2)

@constraint(model, summed,                  y .== s + x)
@constraint(model,  row[i = 1:n-1], sum(y[i, :]) == sum(y[i+1, :]))
@constraint(model,  col[j = 1:n-1], sum(y[:, j]) == sum(y[:, j+1]))
@constraint(model, diag,                 tr(y) == tr(rotr90(y)))

@constraint(model,   rc[k = 1:n-1], sum(y[k, :]) == sum(y[:, k]))
@constraint(model,   rd[k = 1:n-1], sum(y[k, :]) == sum(tr(y)))

@variable(model, x_abs[1:n, 1:n], lower_bound=0, upper_bound=n^2-1)
@constraint(model, x_min, x_abs .>=  x)
@constraint(model, x_max, x_abs .>= -x)
@objective(model, Min, sum(x_abs))

# set_silent(model)
optimize!(model)
# value.(x)
# value.(y)
output = convert(UInt8, objective_value(model))
print(output)
